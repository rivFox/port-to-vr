using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class WorldBuildController : MonoBehaviour
{
    const int worldSize = 11;
    const int worldSizeOffset = worldSize / 2;

    [SerializeField] private GameObject blockPrefab;

    GameObject[] worldData = new GameObject[worldSize * worldSize];

    private void Awake()
    {
        blockPrefab.SetActive(false);
    }

    public void SpawnBlock(int x, int y)
    {
        if (x < 0 || x > worldSize - 1 || y < 0 || y > worldSize - 1)
            return;

        var id = GetId(x, y);
        if (!ReferenceEquals(worldData[id], null))
            return;

        var go = Instantiate(blockPrefab, transform);
        go.transform.localPosition = new Vector3(x - worldSizeOffset, 0.5f, y - worldSizeOffset);
        go.SetActive(true);
        worldData[id] = go;
    }

    public void DespawnBlock(int x, int y)
    {
        var id = GetId(x, y);
        var go = worldData[id];
        if (ReferenceEquals(go, null))
            return;

        Destroy(go);
        worldData[id] = null;
    }

    public static int GetId(int x, int y)
    {
        return x * worldSize + y;
    }

    public (int, int) GetCoordFromWorldSpace(Vector3 worldSpace)
    {
        var local = transform.InverseTransformPoint(worldSpace);
        return (Mathf.RoundToInt(local.x) + worldSizeOffset, Mathf.RoundToInt(local.z) + worldSizeOffset);
    }

#if UNITY_EDITOR
    [ContextMenu("Spawn")]
    public void Spawn()
    {
        SpawnBlock(0, 0);
        SpawnBlock(10, 10);
    }

    [ContextMenu("Despawn")]
    public void Despawn()
    {
        DespawnBlock(0, 0);
        DespawnBlock(10, 10);
    }
#endif
}
