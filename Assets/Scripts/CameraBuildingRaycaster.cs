using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraBuildingRaycaster : MonoBehaviour
{
    [SerializeField] LayerMask layerMask;
    new Camera camera;

    private void Awake()
    {
        camera = GetComponent<Camera>();
    }

    private void Update()
    {
        if(Input.GetMouseButton(0))
        {
            var ray = camera.ScreenPointToRay(Input.mousePosition);
            TryCastToBuildInWorld(ray.origin, ray.direction);
        }
        if (Input.GetMouseButton(1))
        {
            var ray = camera.ScreenPointToRay(Input.mousePosition);
            TryCastToDestroyInWorld(ray.origin, ray.direction);
        }
    }

    private void TryCastToBuildInWorld(Vector3 origin, Vector3 direction)
    {
        Debug.DrawRay(origin, direction * 100, Color.red, 0.1f);
        RaycastHit hit;
        if(Physics.Raycast(origin, direction, out hit, 100f, layerMask, QueryTriggerInteraction.Ignore))
        {
            var world = hit.collider.GetComponentInParent<WorldBuildController>();
            if (ReferenceEquals(world, null))
                return;

            var coord = world.GetCoordFromWorldSpace(hit.point);
            world.SpawnBlock(Mathf.RoundToInt(coord.Item1), Mathf.RoundToInt(coord.Item2));
        }
    }

    private void TryCastToDestroyInWorld(Vector3 origin, Vector3 direction)
    {
        Debug.DrawRay(origin, direction * 100, Color.red, 0.1f);
        RaycastHit hit;
        if (Physics.Raycast(origin, direction, out hit, 100f, layerMask, QueryTriggerInteraction.Ignore))
        {
            var world = hit.collider.GetComponentInParent<WorldBuildController>();
            if (ReferenceEquals(world, null))
                return;

            var coord = world.GetCoordFromWorldSpace(hit.point);
            world.DespawnBlock(Mathf.RoundToInt(coord.Item1), Mathf.RoundToInt(coord.Item2));
        }
    }
}
