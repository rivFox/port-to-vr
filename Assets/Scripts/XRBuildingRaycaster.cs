using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.InputSystem;

public class XRBuildingRaycaster : MonoBehaviour
{
    [SerializeField] LayerMask layerMask;
    [SerializeField] InputActionReference buildActionRef;
    [SerializeField] InputActionReference destroyActionRef;

    private void Awake()
    {
        buildActionRef.action.performed += OnBuildAction;
        destroyActionRef.action.performed += OnDestroyAction;
    }

    private void OnBuildAction(InputAction.CallbackContext callback)
    {
        TryCastToBuildInWorld(transform.position, transform.forward);
    }

    private void OnDestroyAction(InputAction.CallbackContext callback)
    {
        TryCastToDestroyInWorld(transform.position, transform.forward);
    }

    private void TryCastToBuildInWorld(Vector3 origin, Vector3 direction)
    {
        Debug.DrawRay(origin, direction * 100, Color.blue, 0.1f);
        RaycastHit hit;
        if (Physics.Raycast(origin, direction, out hit, 100f, layerMask, QueryTriggerInteraction.Ignore))
        {
            var world = hit.collider.GetComponentInParent<WorldBuildController>();
            if (ReferenceEquals(world, null))
                return;

            var coord = world.GetCoordFromWorldSpace(hit.point);
            world.SpawnBlock(Mathf.RoundToInt(coord.Item1), Mathf.RoundToInt(coord.Item2));
        }
    }

    private void TryCastToDestroyInWorld(Vector3 origin, Vector3 direction)
    {
        Debug.DrawRay(origin, direction * 100, Color.red, 0.1f);
        RaycastHit hit;
        if (Physics.Raycast(origin, direction, out hit, 100f, layerMask, QueryTriggerInteraction.Ignore))
        {
            var world = hit.collider.GetComponentInParent<WorldBuildController>();
            if (ReferenceEquals(world, null))
                return;

            var coord = world.GetCoordFromWorldSpace(hit.point);
            world.DespawnBlock(Mathf.RoundToInt(coord.Item1), Mathf.RoundToInt(coord.Item2));
        }
    }
}
