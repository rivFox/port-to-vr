using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.InputSystem;

public class PlayerController : MonoBehaviour
{
    private CharacterController characterController;
    [SerializeField] private float speed = 5f;
    [SerializeField] InputActionReference moveActionRef;

    private void Awake()
    {
        characterController = GetComponent<CharacterController>();
    }

    private void Update()
    {
        var input = moveActionRef.action.ReadValue<Vector2>();
        characterController.Move(new Vector3(input.x * speed, -9.81f, input.y * speed) * Time.deltaTime);
    }
}
